<?php
/**
 * Booster for WooCommerce - Modules
 *
 * @version 5.3.1
 * @since   3.2.4
 * @author  Pluggabl LLC.
 */

if ( ! defined( 'ABSPATH' ) ) exit;

$wcj_module_files = array(
	'class-wcj-checkout-custom-fields.php',
);

$this->modules = array();
$wcj_modules_dir = WCJ_PLUGIN_PATH . '/includes/';
foreach ( $wcj_module_files as $wcj_module_file ) {
	$module = include_once( $wcj_modules_dir . $wcj_module_file );
	$this->modules[ $module->id ] = $module;
}
$this->modules = apply_filters( 'wcj_modules_loaded', $this->modules );